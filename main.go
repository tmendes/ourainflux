package main

import (
	"flag"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/cheggaaa/pb"
	"github.com/joho/godotenv"
	"gitlab.com/tmendes/ourainflux/client"
	"gitlab.com/tmendes/ourainflux/repo"
)

var days = flag.String("days", "", "days")

func main() {
	flag.Parse()

	err := godotenv.Load(".env")
	if err != nil {
		log.Println("Can't find env file. Looking for env variables")
	}

	ouraToken, envSet := os.LookupEnv("OURARING_API_TOKEN")
	if !envSet {
		log.Println("Missing OURARING_API_TOKEN environment variable")
	}

	ifxDbHost, envSet := os.LookupEnv("IFXDB_HOST")
	if !envSet {
		log.Println("Missing IFXDB_HOST environment variable")
	}

	ifxDbToken, envSet := os.LookupEnv("IFXDB_TOKEN")
	if !envSet {
		log.Println("Missing IFXDB_TOKEN environment variable")
	}

	ifxDbOrg, envSet := os.LookupEnv("IFXD_ORG")
	if !envSet {
		log.Println("Missing IFXD_ORG environment variable")
	}

	ifxDbBucket, envSet := os.LookupEnv("IFXDB_BUCKET")
	if !envSet {
		log.Println("Missing IFXDB_BUCKET environment variable")
	}

	daysToFetch, err := strconv.Atoi(*days)
	if err != nil || daysToFetch <= 0 || daysToFetch > 365 {
		log.Fatalln("invalid days input. Days must be in between 1 and 365")
	}

	if daysToFetch == 1 {
		log.Printf("Fetching data for the last %d day", daysToFetch)
	} else {
		log.Printf("Fetching data for the last %d days", daysToFetch)
	}

	now := time.Now()
	yesterday := now.AddDate(0, 0, -daysToFetch)

	o := client.NewOuraRing(ouraToken)
	i := repo.NewInfluxDb(ifxDbHost, ifxDbToken, ifxDbOrg, ifxDbBucket)

	bar := pb.StartNew(8)
	bar.Prefix("Request Data from Oura Ring Servers: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	dataDailyActivity, _ := o.GetDailyActivity(yesterday, now)
	bar.Increment()

	dataDailyReadiness, _ := o.GetDailyReadiness(yesterday, now)
	bar.Increment()

	dataDailySleep, _ := o.GetDailySleep(yesterday, now)
	bar.Increment()

	dataHeartRate, _ := o.GetHeartRateData(yesterday, now)
	bar.Increment()

	dataSessions, _ := o.GetSesions(yesterday, now)
	bar.Increment()

	dataSleepPeriods, _ := o.GetSleepPeriods(yesterday, now)
	bar.Increment()

	dataTags, _ := o.GetTags(yesterday, now)
	bar.Increment()

	dataWorkout, _ := o.GetWorkout(yesterday, now)
	bar.Increment()
	bar.Finish()

	i.InsertDailyActivity(dataDailyActivity)
	i.InsertDailyReadiness(dataDailyReadiness)
	i.InsertDailySleep(dataDailySleep)
	i.InsertHeartRate(dataHeartRate)
	i.InsertSessions(dataSessions)
	i.InsertSleepPeriods(dataSleepPeriods)
	i.InsertTags(dataTags)
	i.InsetWorkout(dataWorkout)

	i.Close()
}
