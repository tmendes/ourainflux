module gitlab.com/tmendes/ourainflux

go 1.19

require (
	github.com/cheggaaa/pb v1.0.29
	github.com/influxdata/influxdb-client-go/v2 v2.12.0
	github.com/joho/godotenv v1.4.0
)

require (
	github.com/deepmap/oapi-codegen v1.8.2 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/influxdata/line-protocol v0.0.0-20200327222509-2487e7298839 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/sys v0.0.0-20220503163025-988cb79eb6c6 // indirect
)
