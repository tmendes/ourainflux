package client

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/tmendes/ourainflux/models"
)

const (
	HOST               = "https://api.ouraring.com"
	DAILY_ACTIVITY_EP  = "v2/usercollection/daily_activity"
	DAILY_READINESS_EP = "v2/usercollection/daily_readiness"
	DAILY_SLEEP_EP     = "v2/usercollection/daily_sleep"
	HEART_RATE_EP      = "v2/usercollection/heartrate"
	PERSONAL_INFO_EP   = "v2/usercollection/personal_info"
	SESSIONS_EP        = "v2/usercollection/session"
	SLEEP_PERIODS_EP   = "v2/usercollection/sleep"
	TAGS_EP            = "v2/usercollection/tag"
	WORKOUT_END        = "v2/usercollection/workout"
)

type OuraRing struct {
	client *http.Client
	token  string
}

func NewOuraRing(token string) *OuraRing {
	return &OuraRing{
		client: &http.Client{
			Timeout: 10 * time.Second,
		},
		token: fmt.Sprintf("Bearer %s", token),
	}

}

func (o *OuraRing) GetDailyActivity(start, end time.Time) (*models.DailyActivityResponse, *error) {
	req, reqErr := o.prepareRequest(DAILY_ACTIVITY_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.DailyActivityResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetDailyReadiness(start, end time.Time) (*models.DailyReadinessResponse, *error) {
	req, reqErr := o.prepareRequest(DAILY_READINESS_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.DailyReadinessResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetDailySleep(start, end time.Time) (*models.DailySleepDataResponse, *error) {
	req, reqErr := o.prepareRequest(DAILY_SLEEP_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.DailySleepDataResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetHeartRateData(start, end time.Time) (*models.HeartRateResponse, *error) {
	req, reqErr := o.prepareRequest(HEART_RATE_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.HeartRateResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetPersonalInfo(start, end time.Time) (*models.PersonalInfoData, *error) {
	req, reqErr := o.prepareRequest(PERSONAL_INFO_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.PersonalInfoData{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetSesions(start, end time.Time) (*models.SessionResponse, *error) {
	req, reqErr := o.prepareRequest(SESSIONS_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.SessionResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetSleepPeriods(start, end time.Time) (*models.SleepPeriodsResponse, *error) {
	req, reqErr := o.prepareRequest(SLEEP_PERIODS_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.SleepPeriodsResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetTags(start, end time.Time) (*models.TagsResponse, *error) {
	req, reqErr := o.prepareRequest(TAGS_EP, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.TagsResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) GetWorkout(start, end time.Time) (*models.WorkoutResponse, *error) {
	req, reqErr := o.prepareRequest(WORKOUT_END, start, end)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := o.client.Do(req)
	if respErr != nil {
		return nil, &respErr
	}
	defer resp.Body.Close()

	data := models.WorkoutResponse{}

	pError := parseDataFromBody(resp.Body, &data)
	if pError != nil {
		return nil, pError
	}

	return &data, nil
}

func (o *OuraRing) prepareRequest(path string, start, end time.Time) (*http.Request, *error) {
	url := fmt.Sprintf("%s/%s", HOST, path)

	req, reqErr := http.NewRequest(http.MethodGet, url, nil)
	if reqErr != nil {
		return nil, &reqErr
	}

	req.Header.Add("Authorization", o.token)

	startDateTile := ""
	endDateTitle := ""
	startDate := ""
	endDate := ""

	if path == HEART_RATE_EP {
		startDateTile = "start_datetime"
		endDateTitle = "end_datetime"

		startDate = start.UTC().Format("2006-01-02T15:04:05-0700")
		endDate = end.UTC().Format("2006-01-02T15:04:05-0700")
	} else {
		startDateTile = "start_date"
		endDateTitle = "end_date"

		startDate = start.Format("2006-01-02")
		endDate = end.Format("2006-01-02")
	}

	q := req.URL.Query()
	q.Add(endDateTitle, endDate)
	q.Add(startDateTile, startDate)
	req.URL.RawQuery = q.Encode()

	return req, nil
}

func parseDataFromBody[T models.Response](rBody io.ReadCloser, data *T) *error {
	body, readErr := io.ReadAll(rBody)
	if readErr != nil {
		return &readErr
	}

	jsonErr := json.Unmarshal(body, &data)
	if jsonErr != nil {
		fmt.Println(jsonErr.Error())
		return &jsonErr
	}

	return nil
}
