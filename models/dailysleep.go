package models

import "time"

type DailySleepContributors struct {
	DeepSleep   int `json:"deep_sleep"`
	Efficiency  int `json:"efficiency"`
	Latency     int `json:"latency"`
	RemSleep    int `json:"rem_sleep"`
	Restfulness int `json:"restfulness"`
	Timing      int `json:"timing"`
	TotalSleep  int `json:"total_sleep"`
}

type DailySleepData struct {
	Contributors DailySleepContributors `json:"contributors"`
	Day          string                 `json:"day"`
	Score        int                    `json:"score"`
	Timestamp    time.Time              `json:"timestamp"`
}

type DailySleepDataResponse struct {
	Data      []DailySleepData `json:"data"`
	NextToken interface{}      `json:"next_token"`
}
