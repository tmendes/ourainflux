package models

type ReadinessSummary struct {
	Contributors              ReadinessContributors `json:"contributors"`
	Score                     int                   `json:"score"`
	TemperatureDeviation      float64               `json:"temperature_deviation"`
	TemperatureTrendDeviation float64               `json:"temperature_trend_deviation"`
}
