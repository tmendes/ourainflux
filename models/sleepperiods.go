package models

import "time"

type SleepPeriodsData struct {
	AverageBreath       float64          `json:"average_breath"`
	AverageHeartRate    float64          `json:"average_heart_rate"`
	AverageHrv          int              `json:"average_hrv"`
	AwakeTime           int              `json:"awake_time"`
	BedtimeEnd          time.Time        `json:"bedtime_end"`
	BedtimeStart        time.Time        `json:"bedtime_start"`
	Day                 string           `json:"day"`
	DeepSleepDuration   int              `json:"deep_sleep_duration"`
	Efficiency          int              `json:"efficiency"`
	HeartRate           Sample           `json:"heart_rate"`
	Hrv                 Sample           `json:"hrv"`
	Latency             int              `json:"latency"`
	LightSleepDuration  int              `json:"light_sleep_duration"`
	LowBatteryAlert     bool             `json:"low_battery_alert"`
	LowestHeartRate     int              `json:"lowest_heart_rate"`
	Movement30Sec       string           `json:"movement_30_sec"`
	Period              int              `json:"period"`
	Readiness           ReadinessSummary `json:"readiness"`
	ReadinessScoreDelta int              `json:"readiness_score_delta"`
	RemSleepDuration    int              `json:"rem_sleep_duration"`
	RestlessPeriods     int              `json:"restless_periods"`
	SleepPhase5Min      string           `json:"sleep_phase_5_min"`
	SleepScoreDelta     int              `json:"sleep_score_delta"`
	TimeInBed           int              `json:"time_in_bed"`
	TotalSleepDuration  int              `json:"total_sleep_duration"`
	Type                string           `json:"type"`
}

type SleepPeriodsResponse struct {
	Data      []SleepPeriodsData `json:"data"`
	NextToken interface{}        `json:"next_token"`
}
