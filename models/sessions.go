package models

import "time"

type SessionData struct {
	Day                  string    `json:"day"`
	StartDatetime        time.Time `json:"start_datetime"`
	EndDatetime          time.Time `json:"end_datetime"`
	Type                 string    `json:"type"`
	HeartRate            Sample    `json:"heart_rate"`
	HeartRateVariability Sample    `json:"heart_rate_variability"`
	Mood                 string    `json:"mood"`
	MotionCount          Sample    `json:"motion_count"`
}

type SessionResponse struct {
	Data      []SessionData `json:"data"`
	NextToken interface{}   `json:"next_token"`
}
