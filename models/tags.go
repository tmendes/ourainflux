package models

import "time"

type TagsData struct {
	Day       string    `json:"day"`
	Text      string    `json:"text"`
	Timestamp time.Time `json:"timestamp"`
	Tags      []string  `json:"tags"`
}

type TagsResponse struct {
	Data      []TagsData  `json:"data"`
	NextToken interface{} `json:"next_token"`
}
