package models

import "time"

type HeartRateData struct {
	Bpm       int       `json:"bpm"`
	Source    string    `json:"source"`
	Timestamp time.Time `json:"timestamp"`
}

type HeartRateResponse struct {
	Data      []HeartRateData `json:"data"`
	NextToken interface{}     `json:"next_token"`
}
