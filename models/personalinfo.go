package models

type PersonalInfoData struct {
	Age           int     `json:"age"`
	Weight        float64 `json:"weight"`
	Height        float64 `json:"height"`
	BiologicalSex string  `json:"biological_sex"`
	Email         string  `json:"email"`
}
