package models

import "time"

type Contributors struct {
	MeetDailyTargets  int `json:"meet_daily_targets"`
	MoveEveryHour     int `json:"move_every_hour"`
	RecoveryTime      int `json:"recovery_time"`
	StayActive        int `json:"stay_active"`
	TrainingFrequency int `json:"training_frequency"`
	TrainingVolume    int `json:"training_volume"`
}

type DailyActivityData struct {
	Class5Min                 string       `json:"class_5_min"`
	Score                     int          `json:"score"`
	ActiveCalories            int          `json:"active_calories"`
	AverageMetMinutes         float64      `json:"average_met_minutes"`
	Contributors              Contributors `json:"contributors"`
	EquivalentWalkingDistance int          `json:"equivalent_walking_distance"`
	HighActivityMetMinutes    int          `json:"high_activity_met_minutes"`
	HighActivityTime          int          `json:"high_activity_time"`
	InactivityAlerts          int          `json:"inactivity_alerts"`
	LowActivityMetMinutes     int          `json:"low_activity_met_minutes"`
	LowActivityTime           int          `json:"low_activity_time"`
	MediumActivityMetMinutes  int          `json:"medium_activity_met_minutes"`
	MediumActivityTime        int          `json:"medium_activity_time"`
	Met                       Sample       `json:"met"`
	MetersToTarget            int          `json:"meters_to_target"`
	NonWearTime               int          `json:"non_wear_time"`
	RestingTime               int          `json:"resting_time"`
	SedentaryMetMinutes       int          `json:"sedentary_met_minutes"`
	SedentaryTime             int          `json:"sedentary_time"`
	Steps                     int          `json:"steps"`
	TargetCalories            int          `json:"target_calories"`
	TargetMeters              int          `json:"target_meters"`
	TotalCalories             int          `json:"total_calories"`
	Day                       string       `json:"day"`
	Timestamp                 time.Time    `json:"timestamp"`
}

type DailyActivityResponse struct {
	Data      []DailyActivityData `json:"data"`
	NextToken interface{}         `json:"next_token"`
}
