package models

import "time"

type ReadinessContributors struct {
	ActivityBalance     int    `json:"activity_balance"`
	BodyTemperature     int    `json:"body_temperature"`
	HrvBalance          *int64 `json:"hrv_balance"`
	PreviousDayActivity int    `json:"previous_day_activity"`
	PreviousNight       int    `json:"previous_night"`
	RecoveryIndex       int    `json:"recovery_index"`
	RestingHeartRate    int    `json:"resting_heart_rate"`
	SleepBalance        int    `json:"sleep_balance"`
}

type DailyReadinessData struct {
	Contributors              ReadinessContributors `json:"contributors"`
	Day                       string                `json:"day"`
	Score                     int                   `json:"score"`
	TemperatureDeviation      float64               `json:"temperature_deviation"`
	TemperatureTrendDeviation float64               `json:"temperature_trend_deviation"`
	Timestamp                 time.Time             `json:"timestamp"`
}

type DailyReadinessResponse struct {
	Data      []DailyReadinessData `json:"data"`
	NextToken interface{}          `json:"next_token"`
}
