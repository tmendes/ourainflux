package models

type Response interface {
	DailyActivityResponse |
		DailyReadinessResponse |
		DailySleepDataResponse |
		HeartRateResponse |
		PersonalInfoData |
		SessionResponse |
		SleepPeriodsResponse |
		TagsResponse |
		WorkoutResponse
}
