package models

import "time"

type WorkoutData struct {
	Activity      string    `json:"activity"`
	Calories      float64   `json:"calories"`
	Day           string    `json:"day"`
	Distance      float64   `json:"distance"`
	EndDatetime   time.Time `json:"end_datetime"`
	Intensity     string    `json:"intensity"`
	Label         string    `json:"label"`
	Source        string    `json:"source"`
	StartDatetime time.Time `json:"start_datetime"`
}

type WorkoutResponse struct {
	Data      []WorkoutData `json:"data"`
	NextToken interface{}   `json:"next_token"`
}
