package repo

import (
	"context"
	"log"
	"time"

	"github.com/cheggaaa/pb"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"gitlab.com/tmendes/ourainflux/models"
)

type InfluxDb struct {
	client influxdb2.Client
	org    string
	bucket string
}

func NewInfluxDb(host, token, org, bucket string) *InfluxDb {
	return &InfluxDb{
		client: influxdb2.NewClientWithOptions(host, token, influxdb2.DefaultOptions().SetBatchSize(100)),
		org:    org,
		bucket: bucket,
	}
}

func (i *InfluxDb) Close() {
	i.client.Close()
}

func (i *InfluxDb) InsertDailyActivity(data *models.DailyActivityResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Daily Activty to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)

	for _, v := range data.Data {
		pDA := influxdb2.NewPoint("daily_activity",
			map[string]string{},
			map[string]interface{}{
				"score":                       v.Score,
				"active_calories":             v.ActiveCalories,
				"avarage_met_minutes":         v.AverageMetMinutes,
				"equivalent_walking_distance": v.EquivalentWalkingDistance,
				"high_activity_met_minutes":   v.HighActivityMetMinutes,
				"high_activity_time":          v.HighActivityTime,
				"inactive_alerts":             v.InactivityAlerts,
				"low_activity_time":           v.LowActivityTime,
				"medium_activity_time":        v.MediumActivityTime,
				"meters_to_target":            v.MetersToTarget,
				"non_wear_time":               v.NonWearTime,
				"sedentary_net_minutes":       v.SedentaryMetMinutes,
				"sedentary_time":              v.SedentaryTime,
				"step":                        v.Steps,
				"target_calories":             v.TargetCalories,
				"target_meters":               v.TargetMeters,
				"total_calories":              v.TotalCalories,
			},
			v.Timestamp)

		if err := writeAPI.WritePoint(context.Background(), pDA); err != nil {
			log.Fatal(err)
		}

		c := v.Contributors
		pC := influxdb2.NewPoint("daily_activity_contributors",
			map[string]string{},
			map[string]interface{}{
				"meet_daily_targets": c.MeetDailyTargets,
				"move_every_houer":   c.MoveEveryHour,
				"recovery_time":      c.RecoveryTime,
				"stay_active":        c.StayActive,
				"training_frequency": c.TrainingFrequency,
				"training_volume":    c.TrainingVolume,
			},
			v.Timestamp)

		if err := writeAPI.WritePoint(context.Background(), pC); err != nil {
			log.Fatal(err)
		}

		m := v.Met
		baseTimestamp := m.Timestamp
		for _, mV := range m.Items {
			pM := influxdb2.NewPoint("daily_activity_met",
				map[string]string{},
				map[string]interface{}{
					"value": mV,
				},
				baseTimestamp)
			if err := writeAPI.WritePoint(context.Background(), pM); err != nil {
				log.Fatal(err)
			}
			baseTimestamp = baseTimestamp.Add(time.Duration(m.Interval) * time.Second)
		}
		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}

func (i *InfluxDb) InsertDailyReadiness(data *models.DailyReadinessResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Daily Readiness to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)

	for _, v := range data.Data {
		pD := influxdb2.NewPoint("daily_readiness",
			map[string]string{}, map[string]interface{}{
				"score":                 v.Score,
				"temperature_deviation": v.TemperatureDeviation,
			}, v.Timestamp)
		if err := writeAPI.WritePoint(context.Background(), pD); err != nil {
			log.Fatal(err)
		}

		c := v.Contributors
		pC := influxdb2.NewPoint("daily_readiness_contributors",
			map[string]string{},
			map[string]interface{}{
				"activity_balance":      c.ActivityBalance,
				"body_temperature":      c.BodyTemperature,
				"hrv_balance":           c.HrvBalance,
				"previous_day_activity": c.PreviousDayActivity,
				"recovery_index":        c.RecoveryIndex,
				"previous_night":        c.PreviousNight,
				"resting_heart_rate":    c.RestingHeartRate,
				"sleep_balance":         c.SleepBalance,
			}, v.Timestamp)
		if err := writeAPI.WritePoint(context.Background(), pC); err != nil {
			log.Fatal(err)
		}

		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}

func (i *InfluxDb) InsertHeartRate(data *models.HeartRateResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Heart Rate to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)

	for _, v := range data.Data {
		pH := influxdb2.NewPoint("heartrate",
			map[string]string{"source": v.Source},
			map[string]interface{}{
				"bpm": v.Bpm,
			},
			v.Timestamp)
		if err := writeAPI.WritePoint(context.Background(), pH); err != nil {
			log.Fatal(err)
		}
		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}

func (i *InfluxDb) InsertDailySleep(data *models.DailySleepDataResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Daily Sleep to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)

	for _, v := range data.Data {
		pD := influxdb2.NewPoint("daily_sleep",
			map[string]string{},
			map[string]interface{}{
				"score": v.Score,
			},
			v.Timestamp)
		if err := writeAPI.WritePoint(context.Background(), pD); err != nil {
			log.Fatal(err)
		}

		c := v.Contributors
		pC := influxdb2.NewPoint("daily_sleep_contributors",
			map[string]string{},
			map[string]interface{}{
				"deep_sleep":  c.DeepSleep,
				"efficiency":  c.Efficiency,
				"latency":     c.Latency,
				"rem_sleep":   c.RemSleep,
				"restfulness": c.Restfulness,
				"timing":      c.Timing,
				"total_sleep": c.TotalSleep,
			},
			v.Timestamp)

		if err := writeAPI.WritePoint(context.Background(), pC); err != nil {
			log.Fatal(err)
		}

		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}

func (i *InfluxDb) InsertSessions(data *models.SessionResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Sessions to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)

	for _, v := range data.Data {
		pS := influxdb2.NewPoint("session",
			map[string]string{
				"mood": v.Mood,
				"type": v.Type,
			},
			map[string]interface{}{
				"start_date_time":        v.StartDatetime,
				"end_date_time":          v.EndDatetime,
				"heart_rate":             v.HeartRate,
				"heart_rate_variability": v.HeartRateVariability,
				"motion_count":           v.MotionCount,
			}, v.StartDatetime)
		if err := writeAPI.WritePoint(context.Background(), pS); err != nil {
			log.Fatal(err)
		}
		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}

func (i *InfluxDb) InsertSleepPeriods(data *models.SleepPeriodsResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Sleep Periods to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)
	for _, v := range data.Data {
		pSP := influxdb2.NewPoint("sleep_periods",
			map[string]string{
				"type": v.Type,
			},
			map[string]interface{}{
				"avarage_breath":        v.AverageBreath,
				"avarage_heart_rate":    v.AverageHeartRate,
				"avarage_hrv":           v.AverageHrv,
				"awake_time":            v.AwakeTime,
				"bed_time_start":        v.BedtimeStart,
				"bed_time_end":          v.BedtimeEnd,
				"deep_sleep_duration":   v.DeepSleepDuration,
				"efficiency":            v.Efficiency,
				"latency":               v.Latency,
				"light_sleep_duration":  v.LightSleepDuration,
				"low_battery_alert":     v.LowBatteryAlert,
				"lowest_heart_rate":     v.LowestHeartRate,
				"movement_30_sec":       v.Movement30Sec,
				"period":                v.Period,
				"readiness_score_delta": v.ReadinessScoreDelta,
				"rem_sleep_duration":    v.RemSleepDuration,
				"restless_periods":      v.RestlessPeriods,
				"sleep_phase5_min":      v.SleepPhase5Min,
				"sleep_score_delta":     v.SleepScoreDelta,
				"time_in_bed":           v.TimeInBed,
				"total_sleep_duration":  v.TotalSleepDuration,
			}, v.BedtimeStart)
		if err := writeAPI.WritePoint(context.Background(), pSP); err != nil {
			log.Fatal(err)
		}

		hr := v.HeartRate
		baseTimestamp := hr.Timestamp
		for _, hrV := range hr.Items {
			pHR := influxdb2.NewPoint("sleep_periods_heart_rate",
				map[string]string{
					"type": v.Type,
				},
				map[string]interface{}{
					"value": hrV,
				},
				baseTimestamp)
			if err := writeAPI.WritePoint(context.Background(), pHR); err != nil {
				log.Fatal(err)
			}
			baseTimestamp = baseTimestamp.Add(time.Duration(hr.Interval) * time.Second)
		}

		hrv := v.Hrv
		baseTimestamp = hrv.Timestamp
		for _, hrvV := range hrv.Items {
			pHrv := influxdb2.NewPoint("sleep_periods_hrv",
				map[string]string{
					"type": v.Type,
				},
				map[string]interface{}{
					"value": hrvV,
				},
				baseTimestamp)
			if err := writeAPI.WritePoint(context.Background(), pHrv); err != nil {
				log.Fatal(err)
			}
			baseTimestamp = baseTimestamp.Add(time.Duration(hr.Interval) * time.Second)
		}

		rs := v.Readiness
		pRs := influxdb2.NewPoint("sleep_periods_readiness_summary",
			map[string]string{
				"type": v.Type,
			},
			map[string]interface{}{
				"score":                 rs.Score,
				"temperature_deviation": rs.TemperatureDeviation,
			}, v.BedtimeStart)
		if err := writeAPI.WritePoint(context.Background(), pRs); err != nil {
			log.Fatal(err)
		}

		rsC := rs.Contributors
		pRsC := influxdb2.NewPoint("sleep_periods_readiness_summary_contributors",
			map[string]string{
				"type": v.Type,
			},
			map[string]interface{}{
				"activity_balance":      rsC.ActivityBalance,
				"body_temperature":      rsC.BodyTemperature,
				"hrv_balance":           rsC.HrvBalance,
				"previous_day_activity": rsC.PreviousDayActivity,
				"previous_night":        rsC.PreviousNight,
				"recovery_index":        rsC.RecoveryIndex,
				"resting_heart_rate":    rsC.RestingHeartRate,
				"sleep_balance":         rsC.SleepBalance,
			}, v.BedtimeStart)
		if err := writeAPI.WritePoint(context.Background(), pRsC); err != nil {
			log.Fatal(err)
		}
		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}

func (i *InfluxDb) InsertTags(data *models.TagsResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Tags to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)

	for _, v := range data.Data {
		for _, tag := range v.Tags {
			pT := influxdb2.NewPoint("tags",
				map[string]string{},
				map[string]interface{}{"tag": tag}, v.Timestamp)
			if err := writeAPI.WritePoint(context.Background(), pT); err != nil {
				log.Fatal(err)
			}
		}
		pTt := influxdb2.NewPoint("tags_text",
			map[string]string{},
			map[string]interface{}{"text": v.Text}, v.Timestamp)
		if err := writeAPI.WritePoint(context.Background(), pTt); err != nil {
			log.Fatal(err)
		}

		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}

func (i *InfluxDb) InsetWorkout(data *models.WorkoutResponse) {
	if data == nil || data.Data == nil {
		return
	}

	bar := pb.StartNew(len(data.Data))
	bar.Prefix("Workout to DB: ")
	bar.ShowBar = true
	bar.ShowPercent = true

	writeAPI := i.client.WriteAPIBlocking(i.org, i.bucket)

	for _, v := range data.Data {
		pW := influxdb2.NewPoint("workout",
			map[string]string{
				"source":    v.Source,
				"intensity": v.Intensity,
				"activity":  v.Activity,
			},
			map[string]interface{}{
				"calories":        v.Calories,
				"distance":        v.Distance,
				"start_date_time": v.StartDatetime,
				"end_date_time":   v.EndDatetime,
				"label":           v.Label,
			}, v.StartDatetime)
		if err := writeAPI.WritePoint(context.Background(), pW); err != nil {
			log.Fatal(err)
		}

		bar.Increment()
	}

	writeAPI.Flush(context.Background())
	bar.Finish()
}
